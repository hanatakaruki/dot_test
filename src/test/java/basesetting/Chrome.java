package basesetting;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;


public class Chrome {
	
	WebDriver driver;
	private FluentWait<WebDriver> myWaitVar;
	String expectedTitle = "iProp.io - Dashboard";
	String actualTitle = "";

	@SuppressWarnings("deprecation")
	public void OpenWebsite() {
		//change with your chromedriver location
		System.setProperty("webdriver.chrome.driver","/Users/ahmadubaidilah/Documents/Lib/chromedriver");
		ChromeOptions options = new ChromeOptions();
        options.addArguments("headless");
        options.addArguments("window-size=1200x600");
		//use it when run in headless
        //driver = new ChromeDriver(options);
        driver = new ChromeDriver();
		myWaitVar = new WebDriverWait (driver,30);
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        String baseUrl = "http://kmssmi.dot.co.id";
        driver.get(baseUrl);
		
	}
	
	@SuppressWarnings("deprecation")
	public void OpenWebsiteLogin() {
		//change with your chromedriver location
		System.setProperty("webdriver.chrome.driver","/Users/ahmadubaidilah/Documents/Lib/chromedriver");
		ChromeOptions options = new ChromeOptions();
        options.addArguments("headless");
        options.addArguments("window-size=1200x600");
		//use it when run in headless
        //driver = new ChromeDriver(options);
        driver = new ChromeDriver();
		myWaitVar = new WebDriverWait (driver,30);
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        String baseUrl = "http://kmssmi.dot.co.id/raw/login";
        driver.get(baseUrl);
		
	}
	

	public void RegisterwithEmptyFullname() {
		driver.findElement(By.xpath("//a[contains(text(),'Daftar Akun')]")).click();
		driver.findElement(By.id("fullname")).sendKeys("");
		driver.findElement(By.id("institution")).sendKeys("geniebook");
		driver.findElement(By.id("email")).sendKeys("ahmad.ubaidillah2@gmail.com");
		driver.findElement(By.id("pass1")).sendKeys("@password1");
		driver.findElement(By.id("pass2")).sendKeys("@password1");
		driver.findElement(By.id("register")).click();
		
	}

	public void RegisterwithEmptyInstitusi() {
		driver.findElement(By.xpath("//a[contains(text(),'Daftar Akun')]")).click();
		driver.findElement(By.id("fullname")).sendKeys("ahmad ubaidillah");
		driver.findElement(By.id("institution")).sendKeys("");
		driver.findElement(By.id("email")).sendKeys("ahmad.ubaidillah2@gmail.com");
		driver.findElement(By.id("pass1")).sendKeys("@password1");
		driver.findElement(By.id("pass2")).sendKeys("@password1");
		driver.findElement(By.id("register")).click();
		
	}

	public void RegisterwithEmptyPassword() {
		driver.findElement(By.xpath("//a[contains(text(),'Daftar Akun')]")).click();
		driver.findElement(By.id("fullname")).sendKeys("ahmad ubaidillah");
		driver.findElement(By.id("institution")).sendKeys("geniebook");
		driver.findElement(By.id("email")).sendKeys("ahmad.ubaidillah2@gmail.com");
		driver.findElement(By.id("pass1")).sendKeys("");
		driver.findElement(By.id("pass2")).sendKeys("@password1");
		driver.findElement(By.id("register")).click();
		
	}

	public void RegisterwithEmptyEmail() {
		driver.findElement(By.xpath("//a[contains(text(),'Daftar Akun')]")).click();
		driver.findElement(By.id("fullname")).sendKeys("ahmad ubaidillah");
		driver.findElement(By.id("institution")).sendKeys("geniebook");
		driver.findElement(By.id("email")).sendKeys("");
		driver.findElement(By.id("pass1")).sendKeys("@password1");
		driver.findElement(By.id("pass2")).sendKeys("@password1");
		driver.findElement(By.id("register")).click();
		
	}

	public void RegisterwithEmptyrePassword() {
		driver.findElement(By.xpath("//a[contains(text(),'Daftar Akun')]")).click();
		driver.findElement(By.id("fullname")).sendKeys("ahmad ubaidillah");
		driver.findElement(By.id("institution")).sendKeys("geniebook");
		driver.findElement(By.id("email")).sendKeys("ahmad.ubaidillah2@gmail.com");
		driver.findElement(By.id("pass1")).sendKeys("@password1");
		driver.findElement(By.id("pass2")).sendKeys("");
		driver.findElement(By.id("register")).click();
		
	}

	public void RegisterwithWrongemailFormat() {
		driver.findElement(By.xpath("//a[contains(text(),'Daftar Akun')]")).click();
		driver.findElement(By.id("fullname")).sendKeys("ahmad ubaidillah");
		driver.findElement(By.id("institution")).sendKeys("geniebook");
		driver.findElement(By.id("email")).sendKeys("gmail.com");
		driver.findElement(By.id("pass1")).sendKeys("@password1");
		driver.findElement(By.id("pass2")).sendKeys("@password1");
		driver.findElement(By.id("register")).click();
		
	}

	public void RegisterwithValidData() {
		driver.findElement(By.xpath("//a[contains(text(),'Daftar Akun')]")).click();
		driver.findElement(By.id("fullname")).sendKeys("ahmad ubaidillah");
		driver.findElement(By.id("institution")).sendKeys("geniebook");
		driver.findElement(By.id("email")).sendKeys("ahmad.ubaidillah2@gmail.com");
		driver.findElement(By.id("pass1")).sendKeys("@password1");
		driver.findElement(By.id("pass2")).sendKeys("@password1");
		driver.findElement(By.id("register")).click();
		
	}
	

	public void Failedtosignup() {
		myWaitVar.until(ExpectedConditions.visibilityOfElementLocated(By.id("register")));
		driver.quit();
		
	}

	public void Successtosignup() throws InterruptedException {
		try {
	        @SuppressWarnings("deprecation")
			WebDriverWait wait = new WebDriverWait(driver, 2);
	        wait.until(ExpectedConditions.alertIsPresent());
	        Alert alert = driver.switchTo().alert();
	        alert.accept();
	        driver.quit();
	    } catch (Exception e) {
	    	driver.quit();
	    }
		
	}

	public boolean isTextOnPagePresent(String text) {
	    WebElement body = driver.findElement(By.tagName("body"));
	    String bodyText = body.getText();
	    return bodyText.contains(text);
	}
	
	public void LoginwithEmptyEmail() {
		driver.findElement(By.id("email")).sendKeys("");
		driver.findElement(By.id("password")).sendKeys("@password1");
		driver.findElement(By.id("password")).sendKeys("submitButtonLogin");
		
	}


	public void FailedtoLogin() {
		isTextOnPagePresent("Tolong isi email Anda!");
		driver.quit();
		
	}


	public void Successtologin() {
		// TODO Auto-generated method stub
		driver.quit();
		
	}

	public void LoginwithEmptyPassword() {
		driver.findElement(By.id("email")).sendKeys("ahmad.ubaidillah2@gmail.com");
		driver.findElement(By.id("password")).sendKeys("");
		driver.findElement(By.id("password")).sendKeys("submitButtonLogin");
		
	}

	public void LoginwithWrongemailformat() {
		driver.findElement(By.id("email")).sendKeys("gmail.com");
		driver.findElement(By.id("password")).sendKeys("@password1");
		driver.findElement(By.id("password")).sendKeys("submitButtonLogin");
		
	}

	public void LoginwithWrongpasswordformat() {
		driver.findElement(By.id("email")).sendKeys("ahmad.ubaidillah2@gmail.com");
		driver.findElement(By.id("password")).sendKeys("1 2 3");
		driver.findElement(By.id("password")).sendKeys("submitButtonLogin");
		
	}

	public void Loginwithwrongemail() {
		driver.findElement(By.id("email")).sendKeys("wrong@email.com");
		driver.findElement(By.id("password")).sendKeys("@password1");
		driver.findElement(By.id("password")).sendKeys("submitButtonLogin");
		
	}

	public void LoginwithWrongpassword() {
		driver.findElement(By.id("email")).sendKeys("ahmad.ubaidillah2@gmail.com");
		driver.findElement(By.id("password")).sendKeys("wrongpassword");
		driver.findElement(By.id("password")).sendKeys("submitButtonLogin");
		
	}

	public void Loginvaliddata() {
		driver.findElement(By.id("email")).sendKeys("ahmad.ubaidillah2@gmail.com");
		driver.findElement(By.id("password")).sendKeys("@password1");
		driver.findElement(By.id("password")).sendKeys("submitButtonLogin");
		
	}





	}

