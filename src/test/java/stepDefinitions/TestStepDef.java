package stepDefinitions;

import basesetting.Chrome;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;


public class TestStepDef {

	Chrome ch= new Chrome();
	
	@Given("Open kmssmi web")
	public void open_kmssmi_web() throws Exception{
	    // Write code here that turns the phrase above into concrete actions
	    ch.OpenWebsite();
	}
	
	@Given("Open kmssmi web login page")
	public void open_kmssmi_web_login_page() throws Exception{
	    // Write code here that turns the phrase above into concrete actions
	    ch.OpenWebsiteLogin();
	}

	@When("user register with empty full name")
	public void user_register_with_empty_full_name() throws Exception{
	    // Write code here that turns the phrase above into concrete actions
	    ch.RegisterwithEmptyFullname();
	}
	
	@When("user register with empty institusi")
	public void user_register_with_empty_institusi() throws Exception{
	    // Write code here that turns the phrase above into concrete actions
	    ch.RegisterwithEmptyInstitusi();
	}
	
	@When("user register with empty email")
	public void user_register_with_empty_email() throws Exception{
	    // Write code here that turns the phrase above into concrete actions
		ch.RegisterwithEmptyEmail();
	}
	
	@When("user register with empty password")
	public void user_register_with_empty_password() throws Exception{
	    // Write code here that turns the phrase above into concrete actions
		ch.RegisterwithEmptyPassword();
	}
	
	@When("user register with empty repassword")
	public void user_register_with_empty_repassword() throws Exception{
	    // Write code here that turns the phrase above into concrete actions
		ch.RegisterwithEmptyrePassword();
	}
	
	@When("user register with wrong email format")
	public void user_register_with_wrong_email_format() throws Exception{
	    // Write code here that turns the phrase above into concrete actions
		ch.RegisterwithWrongemailFormat();
	}
	
	@When("user register with valid data")
	public void user_register_with_valid_data() throws Exception{
	    // Write code here that turns the phrase above into concrete actions
		ch.RegisterwithValidData();
	}
	
	
	@Then("user should be failed to signup")
	public void user_should_be_failed_to_signup() throws Exception{
	    // Write code here that turns the phrase above into concrete actions
	    ch.Failedtosignup();
	}
	

	@Then("user should be success to signup")
	public void user_should_be_success_to_signup() throws Exception{
	    // Write code here that turns the phrase above into concrete actions
		ch.Successtosignup();
	}
	
	@When("user login with empty email")
	public void user_login_with_empty_email() throws Exception{
	    // Write code here that turns the phrase above into concrete actions
		ch.LoginwithEmptyEmail();
	}
	
	@When("user login with empty password")
	public void user_login_with_empty_password() throws Exception{
	    // Write code here that turns the phrase above into concrete actions
		ch.LoginwithEmptyPassword();
	}
	
	@When("user login with wrong email format")
	public void user_login_with_wrong_email_format() throws Exception{
	    // Write code here that turns the phrase above into concrete actions
		ch.LoginwithWrongemailformat();

	}
	
	@When("user login with wrong password format")
	public void user_login_with_wrong_password_format() throws Exception{
	    // Write code here that turns the phrase above into concrete actions
		ch.LoginwithWrongpasswordformat();
	}
	
	@When("user login with wrong email")
	public void user_login_with_wrong_email() throws Exception{
	    // Write code here that turns the phrase above into concrete actions
		ch.Loginwithwrongemail();
	}
	
	@When("user login with wrong password")
	public void user_login_with_wrong_password() throws Exception{
	    // Write code here that turns the phrase above into concrete actions
		ch.LoginwithWrongpassword();
	}
	
	@When("user login with valid data")
	public void user_login_with_valid_data() throws Exception{
	    // Write code here that turns the phrase above into concrete actions
		ch.Loginvaliddata();
	}
	
	@Then("user should be failed to login")
	public void user_should_be_failed_to_login() throws Exception{
	    // Write code here that turns the phrase above into concrete actions
		ch.FailedtoLogin();
	}
	
	@Then("user should be success to login")
	public void user_should_be_success_to_login() throws Exception{
	    // Write code here that turns the phrase above into concrete actions
		ch.Successtologin();
	}
	



	
}
