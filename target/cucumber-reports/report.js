$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("file:resources/features/1.Signup.feature");
formatter.feature({
  "name": "Signup test scenario",
  "description": "",
  "keyword": "Feature"
});
formatter.scenario({
  "name": "Signup test with empty full name",
  "description": "",
  "keyword": "Scenario"
});
formatter.step({
  "name": "Open kmssmi web",
  "keyword": "Given "
});
formatter.match({
  "location": "TestStepDef.open_kmssmi_web()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user register with empty full name",
  "keyword": "When "
});
formatter.match({
  "location": "TestStepDef.user_register_with_empty_full_name()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user should be failed to signup",
  "keyword": "Then "
});
formatter.match({
  "location": "TestStepDef.user_should_be_failed_to_signup()"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "Signup test with empty institusi",
  "description": "",
  "keyword": "Scenario"
});
formatter.step({
  "name": "Open kmssmi web",
  "keyword": "Given "
});
formatter.match({
  "location": "TestStepDef.open_kmssmi_web()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user register with empty institusi",
  "keyword": "When "
});
formatter.match({
  "location": "TestStepDef.user_register_with_empty_institusi()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user should be failed to signup",
  "keyword": "Then "
});
formatter.match({
  "location": "TestStepDef.user_should_be_failed_to_signup()"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "Signup test with empty email",
  "description": "",
  "keyword": "Scenario"
});
formatter.step({
  "name": "Open kmssmi web",
  "keyword": "Given "
});
formatter.match({
  "location": "TestStepDef.open_kmssmi_web()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user register with empty email",
  "keyword": "When "
});
formatter.match({
  "location": "TestStepDef.user_register_with_empty_email()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user should be failed to signup",
  "keyword": "Then "
});
formatter.match({
  "location": "TestStepDef.user_should_be_failed_to_signup()"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "Signup test with empty password",
  "description": "",
  "keyword": "Scenario"
});
formatter.step({
  "name": "Open kmssmi web",
  "keyword": "Given "
});
formatter.match({
  "location": "TestStepDef.open_kmssmi_web()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user register with empty password",
  "keyword": "When "
});
formatter.match({
  "location": "TestStepDef.user_register_with_empty_password()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user should be failed to signup",
  "keyword": "Then "
});
formatter.match({
  "location": "TestStepDef.user_should_be_failed_to_signup()"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "Signup test with empty repassword",
  "description": "",
  "keyword": "Scenario"
});
formatter.step({
  "name": "Open kmssmi web",
  "keyword": "Given "
});
formatter.match({
  "location": "TestStepDef.open_kmssmi_web()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user register with empty repassword",
  "keyword": "When "
});
formatter.match({
  "location": "TestStepDef.user_register_with_empty_repassword()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user should be failed to signup",
  "keyword": "Then "
});
formatter.match({
  "location": "TestStepDef.user_should_be_failed_to_signup()"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "Signup test with wrong email format",
  "description": "",
  "keyword": "Scenario"
});
formatter.step({
  "name": "Open kmssmi web",
  "keyword": "Given "
});
formatter.match({
  "location": "TestStepDef.open_kmssmi_web()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user register with wrong email format",
  "keyword": "When "
});
formatter.match({
  "location": "TestStepDef.user_register_with_wrong_email_format()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user should be failed to signup",
  "keyword": "Then "
});
formatter.match({
  "location": "TestStepDef.user_should_be_failed_to_signup()"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "Signup test with valid data",
  "description": "",
  "keyword": "Scenario"
});
formatter.step({
  "name": "Open kmssmi web",
  "keyword": "Given "
});
formatter.match({
  "location": "TestStepDef.open_kmssmi_web()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user register with valid data",
  "keyword": "When "
});
formatter.match({
  "location": "TestStepDef.user_register_with_valid_data()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user should be success to signup",
  "keyword": "Then "
});
formatter.match({
  "location": "TestStepDef.user_should_be_success_to_signup()"
});
formatter.result({
  "status": "passed"
});
formatter.uri("file:resources/features/2.Login.feature");
formatter.feature({
  "name": "Login test scenario",
  "description": "",
  "keyword": "Feature"
});
formatter.scenario({
  "name": "Login test with empty email",
  "description": "",
  "keyword": "Scenario"
});
formatter.step({
  "name": "Open kmssmi web login page",
  "keyword": "Given "
});
formatter.match({
  "location": "TestStepDef.open_kmssmi_web_login_page()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user login with empty email",
  "keyword": "When "
});
formatter.match({
  "location": "TestStepDef.user_login_with_empty_email()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user should be failed to login",
  "keyword": "Then "
});
formatter.match({
  "location": "TestStepDef.user_should_be_failed_to_login()"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "Login test with empty password",
  "description": "",
  "keyword": "Scenario"
});
formatter.step({
  "name": "Open kmssmi web login page",
  "keyword": "Given "
});
formatter.match({
  "location": "TestStepDef.open_kmssmi_web_login_page()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user login with empty password",
  "keyword": "When "
});
formatter.match({
  "location": "TestStepDef.user_login_with_empty_password()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user should be failed to login",
  "keyword": "Then "
});
formatter.match({
  "location": "TestStepDef.user_should_be_failed_to_login()"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "Login test with wrong email format",
  "description": "",
  "keyword": "Scenario"
});
formatter.step({
  "name": "Open kmssmi web login page",
  "keyword": "Given "
});
formatter.match({
  "location": "TestStepDef.open_kmssmi_web_login_page()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user login with wrong email format",
  "keyword": "When "
});
formatter.match({
  "location": "TestStepDef.user_login_with_wrong_email_format()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user should be failed to login",
  "keyword": "Then "
});
formatter.match({
  "location": "TestStepDef.user_should_be_failed_to_login()"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "Login test with wrong password format",
  "description": "",
  "keyword": "Scenario"
});
formatter.step({
  "name": "Open kmssmi web login page",
  "keyword": "Given "
});
formatter.match({
  "location": "TestStepDef.open_kmssmi_web_login_page()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user login with wrong password format",
  "keyword": "When "
});
formatter.match({
  "location": "TestStepDef.user_login_with_wrong_password_format()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user should be failed to login",
  "keyword": "Then "
});
formatter.match({
  "location": "TestStepDef.user_should_be_failed_to_login()"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "Login test with wrong email",
  "description": "",
  "keyword": "Scenario"
});
formatter.step({
  "name": "Open kmssmi web login page",
  "keyword": "Given "
});
formatter.match({
  "location": "TestStepDef.open_kmssmi_web_login_page()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user login with wrong email",
  "keyword": "When "
});
formatter.match({
  "location": "TestStepDef.user_login_with_wrong_email()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user should be failed to login",
  "keyword": "Then "
});
formatter.match({
  "location": "TestStepDef.user_should_be_failed_to_login()"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "Login test with wrong password",
  "description": "",
  "keyword": "Scenario"
});
formatter.step({
  "name": "Open kmssmi web login page",
  "keyword": "Given "
});
formatter.match({
  "location": "TestStepDef.open_kmssmi_web_login_page()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user login with wrong password",
  "keyword": "When "
});
formatter.match({
  "location": "TestStepDef.user_login_with_wrong_password()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user should be failed to login",
  "keyword": "Then "
});
formatter.match({
  "location": "TestStepDef.user_should_be_failed_to_login()"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "Login test with valid data",
  "description": "",
  "keyword": "Scenario"
});
formatter.step({
  "name": "Open kmssmi web login page",
  "keyword": "Given "
});
formatter.match({
  "location": "TestStepDef.open_kmssmi_web_login_page()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user login with valid data",
  "keyword": "When "
});
formatter.match({
  "location": "TestStepDef.user_login_with_valid_data()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user should be success to login",
  "keyword": "Then "
});
formatter.match({
  "location": "TestStepDef.user_should_be_success_to_login()"
});
formatter.result({
  "status": "passed"
});
});