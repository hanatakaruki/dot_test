Feature: Signup test scenario

Scenario: Signup test with empty full name
Given Open kmssmi web
When user register with empty full name
Then user should be failed to signup

Scenario: Signup test with empty institusi
Given Open kmssmi web
When user register with empty institusi
Then user should be failed to signup

Scenario: Signup test with empty email
Given Open kmssmi web
When user register with empty email
Then user should be failed to signup

Scenario: Signup test with empty password
Given Open kmssmi web
When user register with empty password
Then user should be failed to signup

Scenario: Signup test with empty repassword
Given Open kmssmi web
When user register with empty repassword
Then user should be failed to signup

Scenario: Signup test with wrong email format
Given Open kmssmi web
When user register with wrong email format
Then user should be failed to signup

Scenario: Signup test with valid data
Given Open kmssmi web
When user register with valid data
Then user should be success to signup