Feature: Login test scenario

Scenario: Login test with empty email
Given Open kmssmi web login page
When user login with empty email
Then user should be failed to login

Scenario: Login test with empty password
Given Open kmssmi web login page
When user login with empty password
Then user should be failed to login

Scenario: Login test with wrong email format
Given Open kmssmi web login page
When user login with wrong email format
Then user should be failed to login

Scenario: Login test with wrong password format
Given Open kmssmi web login page
When user login with wrong password format
Then user should be failed to login

Scenario: Login test with wrong email
Given Open kmssmi web login page
When user login with wrong email
Then user should be failed to login

Scenario: Login test with wrong password
Given Open kmssmi web login page
When user login with wrong password
Then user should be failed to login

Scenario: Login test with valid data
Given Open kmssmi web login page
When user login with valid data
Then user should be success to login